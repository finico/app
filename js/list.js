/**
 *
 * @returns List
 */
Main.getListConst = function () {

    /**
     * @class List
     * @extends P
     */
    var List = Main.getPConst().extend({

        constructor: function (list) {

            this.id = list.id;
            this.name = list.name;
            this.tasks = list.tasks;

            this.init();

        },

        init: function() {
            this.initMediator();
        }

    });

    return List;

};