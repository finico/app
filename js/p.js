/**
 * @method
 * @returns {Class} P
 */
Main.getPConst = function () {

    /**
     * @class P
     */
    var P = new Class({

        /**
         * @method
         * @returns {object} Main
         */
        getMain: function() {
            return Main;
        },

        /**
         * @method
         * @returns {App}
         */
        getApp: function () {
            return Main.app;
        },

        /**
         * @method
         * @this P
         * @return {String}
         */
        genId: function() {
            return Math.random().toString(36).slice(2);
        },

        /**
         * проверка, что параметр массив
         * @method
         * @this P
         * @param {*} param
         * @return {Boolean}
         */
        isArray: function(param) {
            return Array.isArray(param);
        },

        /**
         * проверка, что параметр объект
         * @method
         * @this P
         * @param {*} param
         * @return {Boolean}
         */
        isObject: function(param) {
            return (typeof param === 'object' && !this.isArray(param));
        },

        /**
         * проверка, что параметр функция
         * @method
         * @this P
         * @param {*} param
         * @return {Boolean}
         */
        isFunction: function(param) {
            return typeof param === 'function';
        },

        /**
         * проверка, что параметр число
         * @method
         * @this P
         * @param {*} param
         * @return {Boolean}
         */
        isNumber: function(param) {
            return (!isNaN(parseFloat(param)) && isFinite(param));
        },

        /**
         * проверка, что параметр строка
         * @method
         * @this P
         * @param {*} param
         * @return {Boolean}
         */
        isString: function(param) {
            return typeof param === 'string';
        },

        /**
         * проверка наличия ключа в объекте
         * @method
         * @this P
         * @param {String} key
         * @param {Object} obj
         * @return {Boolean}
         */
        hasKey: function(key, obj) {
            return key in obj;
        },

        /**
         * перебор ключей объекта
         * @method
         * @this P
         * @param {Object} obj
         * @param {Function} func
         */
        itarObj: function(obj, func) {
            for (var key in obj) {
                if ( obj.hasOwnProperty(key) ) {
                    func(obj[key], key);
                }
            }
        },

        /**
         * клонирование объектов
         * @method
         * @this P
         * @param {Object} obj
         * @return {Object}
         */
        cloneObj: function(obj) {
            var that = this;

            return (function clone(o) {
                var tmp = {};
                that.itarObj(o, function (val, key) {
                    if ( that.isObject(val) ) {
                        tmp[key] = clone(val);
                    } else if ( that.isArray(val) ) {
                        tmp[key] = val.slice();
                    } else {
                        tmp[key] = val;
                    }
                });
                return tmp;
            })(obj);
        },

        /**
         * слияние объектов
         * @method
         * @this P
         * @param {Object} newObj
         * @param {Object} obj
         * @return {Object}
         */
        mergeObj: function(newObj, obj) {
            var that = this,
                clone = this.cloneObj(obj);

            return (function merge(newO, o) {
                that.itarObj(newO, function(val, key) {
                    if ( that.hasKey(key, o) ) {
                        if ( that.isObject(o[key]) && that.isObject(val) ) {
                            o[key] = merge(val, o[key]);
                        } else if ( that.isArray(o[key]) && that.isArray(val) ) {
                            o[key] = val.slice();
                        } else {
                            o[key] = val;
                        }
                    } else {
                        o[key] = val;
                    }
                });
                return o;
            })(newObj, clone);
        },

        /**
         * привязка обработчиков
         * @method
         * @this P
         * @param {String|Object} eventName
         * @param {Function} [eventHandler]
         * @return {P|Boolean}
         */
        bindEvent: function(eventName, eventHandler) {
            var that = this;

            if ( this.isObject(eventName) ) {
                that.itarObj(eventName, function(eventName, handler) {
                    that.bindEvent(eventName, handler)
                })
            }

            if ( !(this.isString(eventName)) || this.isNumber(eventName) ) {
                return false;
            }

            if ( !this.isFunction(eventHandler) ) {
                return false;
            }

            if ( !this.events ) {
                this.events = {};
            }

            if ( !this.events[eventName] ) {
                this.events[eventName] = [];
            }

            this.events[eventName].push(eventHandler);

            return this;
        },

        /**
         * снятие обработчиков
         * @method
         * @this P
         * @param {String|Object} eventName
         * @param {Function} [eventHandler]
         * @return {P|Boolean}
         */
        unbindEvent: function(eventName, eventHandler) {
            var that = this;

            if ( !this.hasKey(eventName, this.events) ) {
                this.events = {};

                return false;
            }

            if ( this.isObject(eventName) ) {
                this.itarObj(eventName, function(eventName, handler) {
                    that.unbindEvent(eventName, handler)
                });

                return false;
            }

            if ( eventHandler && !this.isFunction(eventHandler) ) {
                return false;
            }

            if ( this.events && this.events[eventName] ) {
                if ( !eventHandler ) {
                    this.events[eventName] = [];
                } else {
                    for ( var i = 0, l = this.events[eventName].length; i < l; i += 1 ) {
                        if ( this.events[eventName][i] === eventHandler ) {
                            this.events[eventName].splice(i, 1);
                            i -= 1;
                        }
                    }
                }
            }

            return this;
        },

        /**
         * триггер событий
         * @method
         * @this P
         * @param {String} eventName
         * @param {*} [args]
         * @return {P|Boolean}
         */
        trigger: function(eventName, args) {
            if ( !this.isString(eventName) || this.isNumber(eventName) ) {
                return false;
            }

            if ( !this.events || !this.events[eventName] || this.events[eventName].length === 0 ) {
                console.log('на данное событие нет обработчиков');

                return false;
            }

            if ( !this.isArray(args) ) {
                args = [args];
            }

            this.events[eventName].forEach(function(func) {
                func.apply(window, args);
            });

            return this;
        },

        /**
         * инициализация медиатора
         * @method
         * @this P
         * @returns P
         */
        initMediator: function() {
            this.getMain().mediator.installTo(this);

            return this;
        },

        /**
         * добавление html элемента
         * @method
         * @param {String} elem
         * @param {HTMLElement|Node} parent
         * @param {String} [classlist]
         * @param {String} [content]
         * @returns {HTMLElement}
         */
        addElement: function(elem, parent, classlist, content) {
            var d = document,
                el = d.createElement(elem);

            el.className = classlist || '';
            el.innerHTML = content || '';

            parent.appendChild(el);

            return el;
        },

        /**
         * рендеринг элемента
         * @method
         * @param {HTMLElement} elem
         * @param {HTMLElement} parent
         * @returns {P}
         */
        render: function(elem, parent) {

            parent.appendChild(elem);

            return this;
        }

    });

    return P;

};