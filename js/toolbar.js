/**
 * тулбар
 * @returns Toolbar
 */
Main.getToolbar = function () {

    /**
     * @class Toolbar
     * @extends P
     */
    var Toolbar = Main.getPConst().extend({

        constructor: function () {

            this.$ = this.addElement('div', document.querySelector('.app'), 'toolbar');

            this.$logo = this.addElement('a', this.$, 'logo', 'logo');
            this.$search = this.addElement('input', this.$, 'search');
            this.$addTask = this.addElement('a', this.$, 'addtask button', 'Add Task');
            this.$addProject = this.addElement('a', this.$, 'addproject button', 'Add Project');
            this.$addNoteFolder = this.addElement('a', this.$, 'addnotefolder button', 'Add Note Folder');

            this.$account = this.addElement('a', this.$, 'account dropdown', '%username%');
            this.$refresh = this.addElement('a', this.$, 'refresh button', 'Refresh');

            this.init();

        },

        init: function() {
            this.initMediator();
        }

    });

    return Toolbar;

};