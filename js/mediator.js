/**
 * @method
 * @returns {Object}
 */
Main.mediator = function() {

    var

    subscribe = function(channel, fn) {
        if ( !Main.mediator.channels[channel] ) {
            Main.mediator.channels[channel] = [];
        }

        Main.mediator.channels[channel].push({
            context: this,
            callback: fn
        });

        return this;
    },

    unsubscribe = function(channel) {
        if ( Main.mediator.channels[channel] ) {
            Main.mediator.channels[channel] = [];
            delete Main.mediator.channels[channel];
        }
    },

    publish = function(channel) {
        if ( !Main.mediator.channels[channel] ) {
            return false;
        }

        var args = Array.prototype.slice.call(arguments, 1),
            subscription = null;

        for (var i = 0, l = Main.mediator.channels[channel].length; i < l; i += 1) {
            subscription = Main.mediator.channels[channel][i];
            subscription.callback.apply(subscription.context, args);
        }

        return this;
    };

    return {

        channels: {},
        publish: publish,
        subscribe: subscribe,
        unsubscribe: unsubscribe,
        installTo: function(obj) {
            obj.subscribe = subscribe;
            obj.publish = publish;
            obj.unsubscribe = unsubscribe;
        }

    };

}();