/**
 * @method
 * @returns Task
 */
Main.getTaskConst = function () {

    /**
     * @class Task
     * @extends P
     */
    var Task = Main.getPConst().extend({

        /**
         * @constructor
         * @param {Object} task
         */
        constructor: function (task) {

            this.id = task.id || this.genId();
            this.name = task.name;

            this.$ = this.template(this.name);

            this.init();

        },

        init: function() {
            this.initMediator();
        },

        /**
         * шаблон задачи
         * @method
         * @this Task
         * @param name
         * @return {string}
         */
        template: function(name) {
            var tpl = '<div class="task">{{name}}</div>';

            return tpl.replace('{{name}}', name)
        }

    });

    return Task;

};