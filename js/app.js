/**
 * @method
 * @returns {App}
 */
Main.getAppConstructor = function () {

    /**
     * @class App
     * @extends P
     */
    var App = Main.getPConst().extend({

        /**
         * @constructor
         */
        constructor: function () {
            this.name = 'app_name';

            this.lists = {};

            this.$ = this.addElement('div', document.body, 'app');

            this.init();
        },

        /**
         * инициализация
         * @this App
         * @method
         */
        init: function () {
            this.initLists(this.getData());
            this.initMediator();
            this.addToolbar();
        },

        /**
         * получение данных
         * @method
         * @return {Array}
         */
        getData: function() {
            return window.defaultData;
        },

        /**
         * инициализация списков
         * @method
         * @param {Object} data
         * @return {App}
         */
        initLists: function(data) {

            var app = this;

            data.forEach(function (elem) {
                app.addList(elem);
            });

            return this;

        },

        /**
         * получение списка по id
         * @method
         * @param {String} id
         * @return {Object|Boolean}
         */
        getListById: function(id) {
            if ( id in this.lists ) {
                return this.lists[id];
            } else {
                return false;
            }
        },

        /**
         * получение списка по имени
         * @method
         * @param {String} name
         * @return {Object|Boolean}
         */
        getListByName: function(name) {
            for (var key in this.lists) {
                if ( this.lists.hasOwnProperty(key) ) {
                    if ( this.lists[key].name === name ) {
                        return this.lists[key];
                    }
                }
            }

            return false;
        },

        /**
         * добавление списка
         * @method
         * @param {Object} params
         */
        addList: function (params) {
            var list = {
                name: params.name,
                id: params.id || 'l_' + this.genId(),
                tasks: params.tasks || []
            };

            this.lists[list.id] = new (this.getMain().getListConst())(list);
        },

        /**
         * TODO
         * добавление задачи
         * @param listId
         * @param task
         */
        addTask: function (listId, task) {
            if ( !task.id ) {
                task.id = 't_' + this.genId();
            }

            this.getApp().lists[listId][task.id] = new (this.getMain().getTaskConst())(task);
        },

        /**
         * добавление тулбара
         * @method
         * @this App
         */
        addToolbar: function() {
            this.toolbar = new (this.getMain().getToolbar());

            //this.render(this.toolbar.$, this.$);
        }

    });

    return App;

};