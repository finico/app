/**
 * @global
 * @type {Object} Main
 */
var Main = {

    /**
     * P : p
     * App : app
     * List : list
     * Task : task
     */

    /**
     * @method
     */
    init: function () {
        this.app = new (this.getAppConstructor());
        window['app'] = this.app;
    }

};
